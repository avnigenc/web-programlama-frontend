import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { EventLookUp } from "../models/EventLookUp.model";

const routes = {
  createEventLookUp: () =>
    `http://localhost:4000/EventLookUp/createEventLookUp`,
  getEventsById: () => `http://localhost:4000/EventLookUp/getEventsById`,
};

@Injectable({
  providedIn: "root",
})
export class EventLookUpService {
  constructor(private http: HttpClient) {}

  public createEvent(eventLookUp: any): Observable<EventLookUp> {
    return this.http.post<EventLookUp>(routes.createEventLookUp(), eventLookUp);
  }

  public getJoinedEventsById(id: number): Observable<EventLookUp[]> {
    const model = {
      Id: id,
    };
    return this.http.post<EventLookUp[]>(routes.getEventsById(), model);
  }
}
