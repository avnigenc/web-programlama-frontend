import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

import { AuthenticateModel } from "../models/ApiModels/Authenticate.model";
import { User } from "../models/User.model";

const routes = {
  register: () => `http://localhost:4000/users/register`,
  login: () => `http://localhost:4000/users/authenticate`,
  updateUser: () => `http://localhost:4000/users/updateUser`,
};

@Injectable({
  providedIn: "root",
})
export class UserService {
  user: User;
  constructor(private http: HttpClient) {}

  public register(user: User): Observable<User> {
    return this.http.post<User>(routes.register(), user);
  }

  public login(authenticateModel: AuthenticateModel): Observable<User> {
    return this.http.post<User>(routes.login(), authenticateModel);
  }

  public updateUser(user: User): Observable<User> {
    return this.http.post<User>(routes.updateUser(), user);
  }

  get isAuth() {
    return !!localStorage.getItem("user");
  }

  getCurrentUser() {
    return localStorage.getItem("user");
  }

  setUser(user: User) {
    localStorage.setItem("user", JSON.stringify(user));
  }

  logout() {
    localStorage.clear();
  }
}
