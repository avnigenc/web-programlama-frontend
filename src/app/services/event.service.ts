import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Event } from "../models/Event.model";
import { Observable } from "rxjs";

const routes = {
  createEvent: () => `http://localhost:4000/event/createEvent`,
  getEventsById: () => `http://localhost:4000/event/getEventsById`,
  getAllEvents: () => `http://localhost:4000/event/getAllEvents`,
};

@Injectable({
  providedIn: "root",
})
export class EventService {
  constructor(private http: HttpClient) {}

  public createEvent(event: Event): Observable<Event> {
    return this.http.post<Event>(routes.createEvent(), event);
  }

  public getAllEvents(): Observable<Event[]> {
    return this.http.get<Event[]>(routes.getAllEvents());
  }

  public getEventsById(id: number): Observable<Event[]> {
    const model = {
      Id: id,
    };
    return this.http.post<Event[]>(routes.getEventsById(), model);
  }
}
