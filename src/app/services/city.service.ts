import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { City } from "../models/City.model";

const routes = {
  getAllCities: () => `http://localhost:4000/city/getAllCities`,
};

@Injectable({
  providedIn: "root",
})
export class CityService {
  constructor(private http: HttpClient) {}

  public getAllCities(): Observable<City[]> {
    return this.http.get<City[]>(routes.getAllCities());
  }
}
