import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { City } from "../models/City.model";
import { Category } from "../models/Category.model";

const routes = {
  getAllCategories: () => `http://localhost:4000/category/getAllCategories`,
};

@Injectable({
  providedIn: "root",
})
export class CategoryService {
  constructor(private http: HttpClient) {}

  public getAllCategories(): Observable<Category[]> {
    return this.http.get<Category[]>(routes.getAllCategories());
  }
}
