export class Event {
  public UserId: number;
  public Name: string;
  public Description: string;
  public StartingDate: Date;
  public Price: number;
  public Location: number;
  public CategoryId: number;
  public CityId: number;
  public Capacity: number;
}
