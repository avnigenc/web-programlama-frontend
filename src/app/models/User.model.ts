import { UserType } from "./UseType.model";

export class User {
  public Id: number;
  public FirstName: string;
  public LastName: string;
  public Email: string;
  public Password: string;
  public createdAt: Date;
  public UserTypeId: number;
  public UserType: UserType;
}
