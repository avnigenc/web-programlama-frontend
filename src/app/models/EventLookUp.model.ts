import { User } from "./User.model";
import { Event } from "./Event.model";

export class EventLookUp {
  public id: number;
  public UserId: number;
  public EventId: number;

  public User: User;
  public Event: Event;
}
