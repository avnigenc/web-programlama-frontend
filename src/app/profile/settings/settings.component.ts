import { Component, OnInit, Inject } from "@angular/core";
import { UserService } from "src/app/services/user.service";
import { User } from "src/app/models/User.model";
import { City } from "src/app/models/City.model";
import { CityService } from "src/app/services/city.service";
import { Category } from "src/app/models/Category.model";
import { CategoryService } from "src/app/services/category.service";
import { Event } from "src/app/models/Event.model";
import { EventService } from "src/app/services/event.service";
import {
  MatDialog,
  MAT_DIALOG_DATA,
  MatDialogRef,
  MatSnackBar,
} from "@angular/material";
import { EventLookUpService } from "src/app/services/event-look-up.service";
import { EventLookUp } from "src/app/models/EventLookUp.model";

export interface DialogData {
  animal: string;
  name: string;
}

@Component({
  selector: "ngx-settings",
  templateUrl: "./settings.component.html",
  styleUrls: ["./settings.component.scss"],
})
export class SettingsComponent implements OnInit {
  displayedColumns: string[] = [
    "id",
    "name",
    "startingDate",
    "city",
    "actions",
  ];
  dataSource: any;
  dataSourceJoined: any;

  user: any;
  isOrganizator: boolean = false;

  organizatorCreateEvent: boolean = false;
  organizatorMyEvents: boolean = false;
  organizatorSettings: boolean = false;
  customerEvents: boolean = false;
  customerSettings: boolean = false;

  cities: City[];
  categories: Category[];

  event: Event;
  events: Event[];
  passwordConfirm: string;
  joinedEvents: EventLookUp[];

  constructor(
    private userService: UserService,
    private cityService: CityService,
    private categoryService: CategoryService,
    private eventService: EventService,
    public dialog: MatDialog,
    private _snackBar: MatSnackBar,
    private eventLookUpService: EventLookUpService
  ) {
    this.event = new Event();
    this.user = userService.getCurrentUser();
    this.user = JSON.parse(this.user);
    if (this.user.userTypeId === 2) {
      this.isOrganizator = true;
      this.organizatorCreateEvent = true;
    } else {
      this.customerEvents = true;
    }
  }

  ngOnInit(): void {
    this.getAllCategories();
    this.getAllCities();
    this.getJoinedEvents();
    this.getEventsById();
  }

  getAllCategories() {
    this.categoryService.getAllCategories().subscribe(
      (response) => {
        this.categories = response;
      },
      (error: any) => {
        console.log(error);
      }
    );
  }

  getAllCities() {
    this.cityService.getAllCities().subscribe(
      (response) => {
        this.cities = response;
        console.log(this.cities);
      },
      (error: any) => {
        console.log(error);
      }
    );
  }

  getEventsById() {
    this.eventService.getEventsById(this.user.id).subscribe(
      (response) => {
        this.events = response;
        this.dataSource = response;
        console.log(this.events);
      },
      (error: any) => {
        console.log(error);
      }
    );
  }

  getJoinedEvents() {
    this.eventLookUpService.getJoinedEventsById(this.user.id).subscribe(
      (response) => {
        console.log(response);
        this.joinedEvents = response;
        this.dataSourceJoined = response;
      },
      (error) => {
        console.log(error);
      }
    );
  }

  changeContentMenu(menuIndex: any) {
    if (menuIndex === 1) {
      this.organizatorCreateEvent = true;
      this.organizatorMyEvents = false;
      this.organizatorSettings = false;
    }
    if (menuIndex === 2) {
      this.organizatorCreateEvent = false;
      this.organizatorMyEvents = true;
      this.organizatorSettings = false;
    }
    if (menuIndex === 3) {
      this.organizatorCreateEvent = false;
      this.organizatorMyEvents = false;
      this.organizatorSettings = true;
    }
    console.log(menuIndex);

    if (menuIndex === 4) {
      console.log("here");
      console.log(this.customerEvents);

      this.customerEvents = true;
      console.log(this.customerEvents);

      this.customerSettings = false;
    }
    if (menuIndex === 5) {
      console.log(this.customerEvents);
      this.customerEvents = false;
      console.log(this.customerEvents);
      console.log(this.customerSettings);

      this.customerSettings = true;
      console.log(this.customerSettings);
    }
  }

  createEventRequest() {
    this.event.UserId = this.user.id;
    console.log(this.event);
    this.eventService.createEvent(this.event).subscribe(
      (response) => {
        console.log(response);
        this.openError("Etkinlik oluşturuldu");
        this.getEventsById();
      },
      (error: any) => {
        console.log(error);
      }
    );
  }

  detailsAndUpdate(item: any): void {
    console.log(item);
    if (item.event) {
      item = item.event;
    }
    const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
      width: "500px",
      data: item,
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log("The dialog was closed");
      console.log(result);
    });
  }

  openError(errorMessage: any) {
    this._snackBar.open(errorMessage, "x", {
      duration: 3000,
    });
    this.event = new Event();
  }

  updateProfile() {
    console.log(this.user);
    if (this.passwordConfirm !== this.user.password) {
      this.openError("Parolalar eşleşmiyor!");
    } else {
      this.userService.updateUser(this.user).subscribe(
        (response) => {
          console.log(response);
          this.openError("Profile bilgileri güncellendi.");
        },
        (error: any) => {
          console.log(error);
        }
      );
      console.log("bişiler olcak şimdi");
    }
  }

  detailJoinedEvent(item: any) {
    console.log(item);
  }
}

@Component({
  selector: "dialog-overview-example-dialog",
  templateUrl: "event-dialog.html",
})
export class DialogOverviewExampleDialog {
  constructor(
    public dialogRef: MatDialogRef<DialogOverviewExampleDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}
