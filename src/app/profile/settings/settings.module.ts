import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import {
  SettingsComponent,
  DialogOverviewExampleDialog,
} from "./settings.component";
import { MatListModule } from "@angular/material/list";
import {
  MatButtonModule,
  MatCardModule,
  MatSelectModule,
  MatIconModule,
} from "@angular/material";
import { MatInputModule } from "@angular/material/input";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { MatNativeDateModule } from "@angular/material/core";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { MatTableModule } from "@angular/material/table";
import { MatDialogModule } from "@angular/material/dialog";
import { MatSnackBarModule } from "@angular/material/snack-bar";

@NgModule({
  declarations: [SettingsComponent, DialogOverviewExampleDialog],
  imports: [
    CommonModule,
    MatListModule,
    MatButtonModule,
    MatInputModule,
    MatCardModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule,
    FormsModule,
    ReactiveFormsModule,
    MatTableModule,
    MatDialogModule,
    MatIconModule,
    MatSnackBarModule,
  ],
  entryComponents: [DialogOverviewExampleDialog],
})
export class SettingsModule {}
