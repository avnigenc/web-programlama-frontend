import { RouterModule, Routes } from "@angular/router";
import { NgModule } from "@angular/core";
import { ProfileComponent } from "./profile.component";
import { SettingsComponent } from "./settings/settings.component";

const routes: Routes = [
  {
    path: "",
    component: SettingsComponent,
    children: [
      {
        path: "",
        redirectTo: "profile",
        pathMatch: "full",
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfileRoutingModule {}
