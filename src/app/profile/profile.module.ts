import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { ProfileRoutingModule } from "./profile-routing.module";
import { ProfileComponent } from "./profile.component";
import { SettingsModule } from "./settings/settings.module";

@NgModule({
  declarations: [ProfileComponent],
  imports: [CommonModule, SettingsModule, ProfileRoutingModule],
})
export class ProfileModule {}
