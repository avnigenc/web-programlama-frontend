import { Component } from "@angular/core";
import { UserService } from "./services/user.service";

@Component({
  selector: "app-root",
  template: `
    <app-header></app-header>
    <router-outlet></router-outlet>
    <app-footer></app-footer>
  `,
})
export class AppComponent {
  title = "frontend";

  constructor(private userService: UserService) {}
}
