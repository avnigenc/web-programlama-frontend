import { Component, OnInit } from "@angular/core";
import { UserService } from "src/app/services/user.service";
import { MatSnackBar } from "@angular/material";
import { AuthenticateModel } from "src/app/models/ApiModels/Authenticate.model";
import { Route } from "@angular/compiler/src/core";
import { Router } from "@angular/router";

@Component({
  selector: "ngx-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
})
export class LoginComponent implements OnInit {
  authenticateModel: AuthenticateModel;
  constructor(
    private userService: UserService,
    private _snackBar: MatSnackBar,
    private router: Router
  ) {
    this.authenticateModel = new AuthenticateModel();
  }
  ngOnInit(): void {}

  login() {
    this.userService.login(this.authenticateModel).subscribe(
      (response: any) => {
        console.log(response);
        if (response.id !== 0) {
          this.userService.setUser(response);
          this.router.navigate(["profile"]);
        }
      },
      (error: any) => {
        console.log(error);
        this.openError("Email veya Parola yanlış!");
      }
    );
  }

  openError(errorMessage: any) {
    this._snackBar.open(errorMessage, "x", {
      duration: 3000,
    });
  }
}
