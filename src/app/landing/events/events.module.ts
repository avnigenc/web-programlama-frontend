import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import {
  EventsComponent,
  DialogOverviewExampleDialog,
} from "./events.component";
import {
  MatCardModule,
  MatListModule,
  MatIcon,
  MatIconModule,
  MatButtonModule,
} from "@angular/material";

@NgModule({
  declarations: [EventsComponent, DialogOverviewExampleDialog],
  imports: [
    CommonModule,
    MatCardModule,
    MatListModule,
    MatIconModule,
    MatButtonModule,
  ],
  entryComponents: [DialogOverviewExampleDialog],
})
export class EventsModule {}
