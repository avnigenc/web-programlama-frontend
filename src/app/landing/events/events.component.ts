import { Component, OnInit, Inject } from "@angular/core";
import { Router } from "@angular/router";
import { EventService } from "src/app/services/event.service";
import {
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatDialog,
  MatSnackBar,
} from "@angular/material";
import { EventLookUpService } from "src/app/services/event-look-up.service";
import { User } from "src/app/models/User.model";
import { UserService } from "src/app/services/user.service";

export interface DialogData {
  animal: string;
  name: string;
}

@Component({
  selector: "ngx-events",
  templateUrl: "./events.component.html",
  styleUrls: ["./events.component.scss"],
})
export class EventsComponent implements OnInit {
  events: Event[];
  constructor(
    private router: Router,
    private eventService: EventService,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.getAllEvents();
  }

  getAllEvents() {
    this.eventService.getAllEvents().subscribe(
      (response: any) => {
        this.events = response;
        console.log(this.events);
      },
      (error: any) => {
        console.log(error);
      }
    );
  }

  openDetails(item: any): void {
    const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
      width: "500px",
      data: item,
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log("The dialog was closed");
      console.log(result);
    });
  }
}

@Component({
  selector: "dialog-overview-example-dialog",
  templateUrl: "event-dialog.html",
})
export class DialogOverviewExampleDialog {
  user: any;
  constructor(
    public dialogRef: MatDialogRef<DialogOverviewExampleDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private eventLookUpService: EventLookUpService,
    private userService: UserService,
    private _snackBar: MatSnackBar
  ) {
    this.user = this.userService.getCurrentUser();
    this.user = JSON.parse(this.user);
    console.log(this.user);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  openError(errorMessage: any) {
    this._snackBar.open(errorMessage, "x", {
      duration: 3000,
    });
    this.onNoClick();
  }

  joinEvent(event: any) {
    console.log(event);
    const eventLookUpModel = {
      UserId: this.user.id,
      EventId: event.id,
    };
    console.log(eventLookUpModel);

    this.eventLookUpService.createEvent(eventLookUpModel).subscribe(
      (response) => {
        console.log(response);
        this.openError("Etkinliğe katılıyorsun!");
      },
      (error: any) => {
        console.log(error);
      }
    );
  }
}
