import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RegisterComponent } from "./register.component";
import {
  MatGridListModule,
  MatCardModule,
  MatIconModule,
  MatInputModule,
  MatButtonModule,
} from "@angular/material";
import { MatTabsModule } from "@angular/material/tabs";
import { MatFormFieldModule } from "@angular/material/form-field";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { UserService } from "src/app/services/user.service";
import { MatSnackBarModule } from "@angular/material/snack-bar";

@NgModule({
  declarations: [RegisterComponent],
  imports: [
    CommonModule,
    MatGridListModule,
    MatTabsModule,
    MatCardModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
    MatIconModule,
    MatInputModule,
    MatButtonModule,
    MatSnackBarModule,
  ],
})
export class RegisterModule {}
