import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { User } from "src/app/models/User.model";
import { UserService } from "src/app/services/user.service";
import { MatSnackBar } from "@angular/material";

@Component({
  selector: "ngx-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.scss"],
})
export class RegisterComponent implements OnInit {
  user: User;
  passwordConfirm: string;
  constructor(
    private router: Router,
    private userService: UserService,
    private _snackBar: MatSnackBar
  ) {
    this.user = new User();
  }

  ngOnInit(): void {}

  openError(errorMessage: any) {
    this._snackBar.open(errorMessage, "x", {
      duration: 3000,
    });
  }

  register(params: any) {
    if (params === "katilimci") {
      this.user.UserTypeId = 1;
    } else {
      this.user.UserTypeId = 2;
    }

    console.log(this.user);
    if (this.passwordConfirm !== this.user.Password) {
      console.log("passwords doesnot match");
      this.openError("Girilen Parolalar Uyuşmuyor.");
    } else {
      this.userService.register(this.user).subscribe(
        (response: any) => {
          console.log(response);
          if (response.isCompletedSuccessfully) {
            this.router.navigate(["login"]);
          }
        },
        (error: any) => {
          console.log(error);
        }
      );
    }
  }
}
