import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { EventService } from "src/app/services/event.service";

@Component({
  selector: "ngx-index",
  templateUrl: "./index.component.html",
  styleUrls: ["./index.component.scss"],
})
export class IndexComponent implements OnInit {
  constructor(private router: Router, private eventService: EventService) {}
  events: any[];
  ngOnInit(): void {
    this.getAllEvents();
  }

  getAllEvents() {
    this.eventService.getAllEvents().subscribe((response) => {
      this.events = response;
      console.log(this.events);
    });
  }

  navigateEvents() {
    this.router.navigate(["events"]);
  }
}
