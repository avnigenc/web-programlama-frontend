import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { IndexComponent } from "./index.component";
import {
  MatGridListModule,
  MatButtonModule,
  MatCardModule,
} from "@angular/material";

@NgModule({
  declarations: [IndexComponent],
  imports: [CommonModule, MatGridListModule, MatButtonModule, MatCardModule],
})
export class IndexModule {}
