import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { LadingRoutingModule } from "./landing-routing.module";

import { MatGridListModule, MatInputModule } from "@angular/material";

import { EventsModule } from "./events/events.module";
import { IndexModule } from "./index/index.module";
import { LoginModule } from "./login/login.module";
import { RegisterModule } from "./register/register.module";
import { LandingComponent } from "./landing.component";
import { UserService } from "../services/user.service";

@NgModule({
  declarations: [LandingComponent],
  imports: [
    LadingRoutingModule,
    CommonModule,
    EventsModule,
    IndexModule,
    LoginModule,
    RegisterModule,
    MatGridListModule,
    MatInputModule,
  ],
  providers: [UserService],
})
export class LandingModule {}
